items = [
    {'id': 1, 'parent': 'root'},
    {'id': 2, 'parent': 1, 'type': 'test'},
    {'id': 3, 'parent': 1, 'type': 'test'},
    {'id': 4, 'parent': 2, 'type': 'test'},
    {'id': 5, 'parent': 2, 'type': 'test'},
    {'id': 6, 'parent': 2, 'type': 'test'},
    {'id': 7, 'parent': 4, 'type': None},
    {'id': 8, 'parent': 4, 'type': None},
]


class TreeStore:
    def __init__(
        self,
        items: list[dict],
    ) -> None:
        self.items = items

    def getAll(self) -> list[dict]:
        return self.items

    def getItem(self, id: int) -> dict:
        return {item.get('id'): item for item in self.items}[id]

    def getChildren(self, id: int) -> list[dict] | list:

        hashed_children = {
            item.get('parent'): tuple(
                child_item
                for child_item in self.items
                if child_item.get('parent') == item.get('parent')
            )
            for item in self.items
        }

        children = hashed_children.get(id)

        if children:
            return list(children)
        return []

    def getAllParents(self, id: int) -> list[dict] | list:
        item = self.getItem(id)
        parent_id = item['parent']
        if parent_id == 'root':
            return []
        parent = self.getItem(parent_id)
        return [parent, *self.getAllParents(parent['id'])]


def main() -> None:
    pass


if __name__ == '__main__':
    main()
