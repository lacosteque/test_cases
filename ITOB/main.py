from app.application import App
from app.client.client import HttpClientRequests
from app.exceptions import HttpClientError, ScraperError
from app.scraper.scraper import ScraperBs4

app = App(
    client=HttpClientRequests(),
    scraper=ScraperBs4(),
)


def main() -> None:
    try:
        result = app.go()
        print(result)
    except HttpClientError as exc:
        print(f'Github page not avalible - {exc}')
    except ScraperError as exc:
        print(f'Scrapy data not avalible - {exc}')


if __name__ == '__main__':
    main()
