from typing import Protocol

from app.client.dto import HtmlDOM


class HttpClient(Protocol):
    def get_request(self, url: str) -> HtmlDOM:
        ...
