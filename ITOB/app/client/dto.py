from typing import NewType

HtmlDOM = NewType('HtmlDOM', bytes)
