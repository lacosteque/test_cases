import requests
from requests.exceptions import ConnectionError, HTTPError

from app.client.dto import HtmlDOM
from app.exceptions import HttpClientError


class HttpClientRequests:
    def __init__(self) -> None:
        self.session = requests.Session()

    def get_request(self, url: str) -> HtmlDOM:
        response = self.session.get(url, timeout=10)
        try:
            response.raise_for_status()
        except (HTTPError, ConnectionError) as exc:
            raise HttpClientError(f'HttpClient error {exc}')
        return HtmlDOM(response.content)
