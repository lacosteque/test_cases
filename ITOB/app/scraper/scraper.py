from bs4 import BeautifulSoup, ResultSet, Tag

from app.client.dto import HtmlDOM
from app.exceptions import ScraperError
from app.scraper.dto import RepoName, RepoStar, RepoUrl


class ScraperBs4:
    def __init__(self) -> None:
        self.soup = BeautifulSoup

    def scrape_items(self, html: HtmlDOM) -> ResultSet:
        scraper = self.soup(html, 'lxml')
        articles = scraper.findAll('article', attrs={'class': 'Box-row'})
        if articles:
            return articles
        raise ScraperError('Can`t scrape RepoArticles')

    def scrape_name(self, tag: Tag) -> RepoName:
        selector = tag.select_one('.lh-condensed')
        if not selector:
            raise ScraperError('Can`t scrape selector')
        if not selector.a:
            raise ScraperError('Can`t scrape link')
        if not selector.a.get_text():
            raise ScraperError('Can`t scrape anchor link')

        return RepoName(selector.a.get_text(strip=True).replace(' ', ''))

    def scrape_stars(self, tag: Tag) -> RepoStar:
        urls = tag.find_all('a')
        try:
            stars = tuple(
                url.get_text(strip=True)
                for url in urls
                if url.find('svg', attrs={'aria-label': 'star'})
            )
        except AttributeError:
            raise ScraperError('Can`t scrape RepoStars')

        if not stars:
            raise ScraperError('Can`t scrape RepoStars')

        return RepoStar(*stars)

    def scrape_url(self, tag: Tag) -> RepoUrl:
        selector = tag.select_one('.lh-condensed')
        if not selector:
            raise ScraperError('Can`t scrape selector')
        if not selector.a:
            raise ScraperError('Can`t scrape link')
        if not selector.a.get('href'):
            raise ScraperError('Can`t scrape href link')

        parts = ['https://github.com', selector.a.get('href')]
        return RepoUrl(''.join(parts))
