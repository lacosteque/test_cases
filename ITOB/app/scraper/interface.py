from typing import Protocol

from bs4 import ResultSet, Tag

from app.client.dto import HtmlDOM
from app.scraper.dto import RepoName, RepoStar, RepoUrl


class Scraper(Protocol):
    def scrape_items(self, html: HtmlDOM) -> ResultSet:
        ...

    def scrape_name(self, tag: Tag) -> RepoName:
        ...

    def scrape_stars(self, tag: Tag) -> RepoStar:
        ...

    def scrape_url(self, tag: Tag) -> RepoUrl:
        ...
