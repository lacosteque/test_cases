from typing import NewType

RepoName = NewType('RepoName', str)
RepoStar = NewType('RepoStar', str)
RepoUrl = NewType('RepoUrl', str)
