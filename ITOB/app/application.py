import json
from typing import TypeAlias

from app.client.interface import HttpClient
from app.scraper.interface import Scraper

Result: TypeAlias = str


class App:
    URL = 'https://github.com/trending'

    def __init__(self, client: HttpClient, scraper: Scraper):
        self.client = client
        self.scraper = scraper

    def go(self) -> Result:
        response = self.client.get_request(self.URL)
        items = self.scraper.scrape_items(response)
        output = json.dumps(
            {
                count: {
                    'name': self.scraper.scrape_name(item),
                    'stars': self.scraper.scrape_stars(item),
                    'url': self.scraper.scrape_url(item),
                }
                for count, item in enumerate(items)
            },
            indent=4,
        )

        return output
