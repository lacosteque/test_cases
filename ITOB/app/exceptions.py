class CustomExceptionError(Exception):
    def __init__(self, message):
        super().__init__(message)


class HttpClientError(CustomExceptionError):
    ...


class ScraperError(CustomExceptionError):
    ...
