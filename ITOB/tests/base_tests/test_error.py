import pytest
from bs4.element import Tag
from pytest_mock import MockerFixture

from app.client.interface import HttpClient
from app.exceptions import ScraperError
from app.scraper.interface import Scraper
from tests.conftest import GithubTrendingResponse


def test_scrape_items_error(
    scraper: Scraper,
    client: HttpClient,
    github_client_mock_error: GithubTrendingResponse,
):
    client.get_request('https://github.com/trending')
    with pytest.raises(ScraperError):
        scraper.scrape_items(github_client_mock_error)


def test_scrape_name_for_first_repo_error(
    scraper: Scraper,
    client: HttpClient,
    github_client_mock: GithubTrendingResponse,
    mocker: MockerFixture,
):
    mocker.patch(
        'app.scraper.scraper.ScraperBs4.scrape_items',
        return_value=[Tag(name='test')],
    )

    client.get_request('https://github.com/trending')
    item = scraper.scrape_items(github_client_mock)
    with pytest.raises(ScraperError):
        scraper.scrape_name(item[0])


def test_scrape_stars_for_first_repo_error(
    scraper: Scraper,
    client: HttpClient,
    github_client_mock: GithubTrendingResponse,
    mocker: MockerFixture,
):
    mocker.patch(
        'app.scraper.scraper.ScraperBs4.scrape_items',
        return_value=[Tag(name='test')],
    )
    client.get_request('https://github.com/trending')
    items = scraper.scrape_items(github_client_mock)
    with pytest.raises(ScraperError):
        scraper.scrape_stars(items[0])


def test_scrape_url_for_first_repo_error(
    scraper: Scraper,
    client: HttpClient,
    github_client_mock: GithubTrendingResponse,
    mocker: MockerFixture,
):
    mocker.patch(
        'app.scraper.scraper.ScraperBs4.scrape_items',
        return_value=[Tag(name='test')],
    )
    client.get_request('https://github.com/trending')
    items = scraper.scrape_items(github_client_mock)
    with pytest.raises(ScraperError):
        scraper.scrape_url(items[0])
