from app.client.interface import HttpClient
from app.scraper.interface import Scraper
from tests.conftest import GithubTrendingResponse


def test_scrape_items(
    github_client_mock: GithubTrendingResponse,
    scraper: Scraper,
    client: HttpClient,
):
    client.get_request('https://github.com/trending')
    items = scraper.scrape_items(github_client_mock)
    assert items is not None


def test_scrape_name_for_first_repo(
    github_client_mock: GithubTrendingResponse,
    scraper: Scraper,
    client: HttpClient,
):

    client.get_request('https://github.com/trending')
    items = scraper.scrape_items(github_client_mock)
    name = scraper.scrape_name(items[0])
    assert name == 'tloen/alpaca-lora'


def test_scrape_stars_for_first_repo(
    github_client_mock: GithubTrendingResponse,
    scraper: Scraper,
    client: HttpClient,
):

    client.get_request('https://github.com/trending')
    items = scraper.scrape_items(github_client_mock)
    stars = scraper.scrape_stars(items[0])
    assert stars == '12,582'


def test_scrape_scrape_url_for_first_repo(
    github_client_mock: GithubTrendingResponse,
    scraper: Scraper,
    client: HttpClient,
):

    client.get_request('https://github.com/trending')
    items = scraper.scrape_items(github_client_mock)
    url = scraper.scrape_url(items[0])
    assert url == 'https://github.com/tloen/alpaca-lora'
