from app.application import App
from app.client.interface import HttpClient
from app.scraper.interface import Scraper


def test_scrape_items(scraper: Scraper, client: HttpClient):
    html = client.get_request('https://github.com/trending')
    items = scraper.scrape_items(html)
    assert items is not None


def test_scrape_name_for_first_repo(scraper: Scraper, client: HttpClient):
    html = client.get_request('https://github.com/trending')
    items = scraper.scrape_items(html)
    name = scraper.scrape_name(items[0])
    assert name is not None


def test_scrape_stars_for_first_repo(scraper: Scraper, client: HttpClient):
    html = client.get_request('https://github.com/trending')
    items = scraper.scrape_items(html)
    stars = scraper.scrape_stars(items[0])
    assert stars is not None


def test_scrape_scrape_url_for_first_repo(
    scraper: Scraper, client: HttpClient
):
    html = client.get_request('https://github.com/trending')
    items = scraper.scrape_items(html)
    url = scraper.scrape_url(items[0])
    assert url is not None


def test_go(scraper: Scraper, client: HttpClient):
    app = App(client=client, scraper=scraper)
    result = app.go()
    assert result is not None
