from typing import Iterator, TypeAlias

import httpretty
import pytest

from app.client.client import HttpClientRequests
from app.client.dto import HtmlDOM
from app.client.interface import HttpClient
from app.scraper.interface import Scraper
from app.scraper.scraper import ScraperBs4

GithubTrendingResponse: TypeAlias = HtmlDOM


@pytest.fixture()
def scraper() -> Iterator[Scraper]:
    yield ScraperBs4()


@pytest.fixture()
def client() -> Iterator[HttpClient]:
    yield HttpClientRequests()


@pytest.fixture()
def github_trending_response() -> GithubTrendingResponse:
    with open(
        './tests/base_tests/trending.html',
        'rb',
    ) as f:
        file_contents = f.read()
    return GithubTrendingResponse(file_contents)


@pytest.fixture()
def github_trending_response_error() -> GithubTrendingResponse:
    return GithubTrendingResponse(b'test')


@pytest.fixture()
def github_client_mock_error(
    github_trending_response_error: GithubTrendingResponse,
) -> Iterator[GithubTrendingResponse]:
    httpretty.enable(verbose=True, allow_net_connect=False)
    httpretty.register_uri(
        method=httpretty.GET,
        uri='https://github.com/trending',
        body=github_trending_response_error.decode('UTF-8'),
    )
    yield github_trending_response_error
    assert httpretty.has_request()


@pytest.fixture()
def github_client_mock(
    github_trending_response: GithubTrendingResponse,
) -> Iterator[GithubTrendingResponse]:
    httpretty.enable(verbose=True, allow_net_connect=False)
    httpretty.register_uri(
        method=httpretty.GET,
        uri='https://github.com/trending',
        body=github_trending_response.decode('UTF-8'),
    )
    yield github_trending_response
    assert httpretty.has_request()
