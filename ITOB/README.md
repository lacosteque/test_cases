# ЗАДАНИЕ
Написать функцию или метод класса, в котором необходимо получить страницу https://github.com/trending и вернуть пронумерованный список в json (название репозитория, количество звёзд, ссылку).

**Требования:** Использовать библиотеки requests, beautifulsoup.  
  
### Результат

       {
    "0": {
        "name": "fathyb/carbonyl",
        "stars": "7,201",
        "url": "https://github.com/fathyb/carbonyl"
    },
    "1": {
        "name": "localsend/localsend",
        "stars": "4,120",
        "url": "https://github.com/localsend/localsend"
    },
    ...
    "24": {
        "name": "DataTalksClub/data-engineering-zoomcamp",
        "stars": "11,694",
        "url": "https://github.com/DataTalksClub/data-engineering-zoomcamp"
    }

